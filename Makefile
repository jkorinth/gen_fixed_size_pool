CC ?= gcc
CFLAGS = -O3 -g -Wall -Werror -std=gnu89 -DUSE_ASSERTIONS
LDFLAGS = -pthread

fsp:	main.c gen_fixed_size_pool.h dbg.h
	$(CC) $(CFLAGS) -o $@ $(LDFLAGS) $<

clean:
	rm -rf fsp fsp.dSYM

