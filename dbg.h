/*
This file is part of gen_fixed_size_pool.

(C) Copyright 2015 Jens Korinth

gen_fixed_size_pool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gen_fixed_size_pool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gen_fixed_size_pool.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __DBG_H__
#define __DBG_H__

#ifndef __cplusplus
#include <stdio.h>
#include <stdlib.h>
#else
#include <cstdio>
#include <cstdlib>
#endif

#define DBG(fmt, ...) 	do { \
	static int _print_debug = 42; \
	if (_print_debug == 42) _print_debug = getenv("FSP_DEBUG") != NULL; \
	if (_print_debug) \
		fprintf(stderr, "[%s]: " fmt, __func__, ##__VA_ARGS__); \
	} while (0);

#endif /* __DBG_H__ */
