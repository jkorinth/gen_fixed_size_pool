/*
This file is part of gen_fixed_size_pool.

(C) Copyright 2015 Jens Korinth

gen_fixed_size_pool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gen_fixed_size_pool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gen_fixed_size_pool.  If not, see <http://www.gnu.org/licenses/>.
*/

//! @file	main.c
//! @brief	Pthread stress test program: Starts one thread per configured
//!		CPU to get and put a fixed number of elements from a single
//!		pool. Pool should never return 0, which is asserted.
//! @authors	J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
//!
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include "gen_fixed_size_pool.h"
#include "dbg.h"

// initialzer for void *'s
static inline void init(void **p, fsp_idx_t const idx) { *p = NULL; (void)idx; }

// make generic list for void *'s
MAKE_FIXED_SIZE_POOL(pvoid, 1024, void *, init)

static void *exercise_list(void *p)
{
	int i;
	struct pvoid_fsp_t *list = (struct pvoid_fsp_t *)p;
	uint32_t const cnt = 10000000; //(uint32_t)rand() % 10000000;
	for (i = 0; i < cnt; ++i) {
		fsp_idx_t const idx = pvoid_fsp_get(list);
		DBG("%u @ %d: got %d\n", (uint32_t)pthread_self(), i, idx);
		assert(idx != (fsp_idx_t)INVALID_IDX);
		pvoid_fsp_put(list, idx);
	}
	return NULL;
}

static void start_test(unsigned int const count, void *arg)
{
	int i;
	pthread_t threads[count];
	DBG("starting %u threads ...\n", count);
	for (i = 0; i < count; ++i)
		pthread_create(&threads[i], NULL, exercise_list, arg);
	for (i = 0; i < count; ++i)
		pthread_join(threads[i], NULL);
}

int main(int argc, char **argv)
{
	struct pvoid_fsp_t my_list;
	long tc;
	srand(time(NULL));
	if (argc > 1)
		tc = strtol(argv[1], NULL, 0);
	else
		tc = sysconf(_SC_NPROCESSORS_CONF);
	printf("Testing with %ld threads.\n", tc);
	pvoid_fsp_init(&my_list);
	start_test(tc, &my_list);
	// start_test(1, &my_list);
	return 0;
}
