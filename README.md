Generic, header-only, lock-free fixed-size pool
===============================================
Header-only C implementation of a lock-free fixed-size pool of things based on a
statically allocated array. Needed this dozens of times, figured implementing
it once and for all would be useful.

Basic Idea
----------
Have statically allocated pool of things which can be acquired and released in a
lock-free manner. Strictly low-level, use only internally to realize an indexed
pool, publish only the indices via your API and use them to access the array.

Usage
-----
Use macro `MAKE_FIXED_SIZE_POOL(PRE, SZ, T, IF)` to instantiate the pool: this
will expand to a type `struct <PRE>_fsp_t` and static functions
`<PRE>_fsp_init`, `<PRE>_fsp_get` and `<PRE>_fsp_put`:
The first must be called once to initialize the array, the former to obtain a
pool index for use and the latter to release the pool index after completion.
`IF` should be a function like `void init_elem(T *, fsp_idx_t const idx)` which
initializes an element. Elements can be accessed directly using the `elems`
member in the struct.

